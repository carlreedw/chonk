--[[
	example_class = {
		name = "ExampleObject",
		description = "To describe how a class would be documented",
		domain = "documentation"
		properties = {
			font = "Font object used before printing",
			color = "Color to set before printing"
		},
		functions = {
			print = {
				signature = "print(message, x = number, y = number)",
				description = "Prints a message (string) at screen pixel coordinate (x, y) (numbers).",
				return = "nil"
			},
			serialize = {
				signature = "serialize(instance)",
				description = "Turns instance into text data",
				return = "a string representing the instance's data"
			}
		}
	}	
]]