--[[
PROJECT: TEMPLATE
AUTHORS: Carl Reed
DATE: 2020-09-03
DESC: a game about flying through space and being a brave adventurer
RUNW: "C:/reedcw1/dev/love.11.3/love.exe" "C:/reedcw1/dev/template"
RUNH: "C:/root/love2d/11.3/love.exe" "C:/root/dev/love2d/template"
AGENDA:
	Document and prototype primary game objects
		Server
		Client
		World
		Map
		Player
		Chonk
		Camera
		Crystal
--]]



function love.load()
	game = love.filesystem.load('game.lua')({
		run_tests = true,
		-- load_logging = true
	})
	
	-- enable quit on esc and debug.debug when press debug.debug_key (f10 default)
	debug.listen(true)
	
	game:push(states.test)
end

function love.update(dt)
	game:update(dt)
end

function love.draw()
	game:draw()
end

