----------------------------
-- extensions for lua math library
----------------------------

local math_abs = math.abs
local math_sqrt = math.sqrt
local math_atan2 = math.atan2 or math.atan

math.clamp = function(x, min, max)
	return x < min and min or (x > max and max or x)
end

math.round = function(num, idp)
	local sign, num = math.sign(num), math.abs(num)
	local mult = 10 ^ (idp or 0)
	return math.floor(num * mult + 0.5) / mult * sign
end

math.sign = function(x)
	return x > 0 and 1 or x < 0 and -1 or 0
end

math.lerp = function(a, b, amount)
  return a + amount * (b - a)
end

math.smooth = function(a, b, amount)
  local t = math.clamp(amount, 0, 1)
  local m = t * t * (3 - 2 * t)
  return a + (b - a) * m
end

math.pingpong = function(x)
  return 1 - math_abs(1 - x % 2)
end

math.distance = function(x1, y1, x2, y2)
	return math_sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1))
end

math.angle = function(x1, y1, x2, y2)
  return math_atan2(y2 - y1, x2 - x1)
end

math.vector = function(angle, magnitude)
  return math.cos(angle) * magnitude, math.sin(angle) * magnitude
end