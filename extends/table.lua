----------------------------
-- extensions for lua table library
----------------------------
table.unpack = unpack or table.unpack
table.weakmetatable = {__mode = "kv"}
local identity = function(x)
	return x
end
local iscallable = function(x)
	if type(x) == "function" then return true end
	local mt = getmetatable(x)
	return mt and mt.__call ~= nil
end
local absindex = function(len, i)
  return i < 0 and (len + i + 1) or i
end
local iteratee = function(x)
	if x == nil then return identity end
	if iscallable(x) then return x end
	if type(x) == "table" then
		return function(z)
			for k, v in pairs(x) do
				if z[k] ~= v then return false end
			end
			return true
		end
	end
	return function(z) return z[x] end
end
local getiter = function(t)
	if table.isarray(t) then
		return ipairs
	elseif type(t) == "table" then
		return pairs
	end
	error("expected table", 3)
end

table.irandomchoice = function(t)
	return t[math.random(#t)]
end

table.randomchoice = function(t)
	local keys = table.keys(t)
	return t[keys[math.random(1, #keys)]]
end

table.weightedchoice = function(t)
	local sum = 0
	
	local iter = getiter(t)
	for _, v in iter(t) do
		assert(v >= 0, "weight value less than zero")
		sum = sum + v
	end
	
	assert(sum ~= 0, "all weights are zero")
	local rnd = math.random() * sum
	for k, v in pairs(t) do
		if rnd < v then return k end
		rnd = rnd - v
	end
end

table.isarray = function(t)
	if type(t) ~= 'table' then return false end
	for k, v in pairs(t) do
		if type(k) ~= 'number' then
			return false
		end
		if math.floor(k) ~= k then
			return false
		end
	end
	return true
end

table.hasarray = function(t)
	if type(t) ~= 'table' then return false end
	return t[1] ~= nil
end

table.push = function(t, ...)
	local n = select("#", ...)
	for i = 1, n do
		t[#t + 1] = select(i, ...)
	end
	return ...
end

table.pop = function(t, ...)	-- not lume.remove(t, ...)
	local n = select("#", ...)
	local ret = {}
	local matched = {}
	local checked = {}
	
	for ri, v in ripairs(t) do
		checked[#checked + 1] = ri
		for i = 1, n do
			if v == select(i, ...) then
				ret[#ret + 1] = table.remove(t, ri)
			end
		end
	end
	
	local iter = getiter(t)
	if iter == ipairs then
		return (unpack or table.unpack)(ret)
	end
	
	for k, v in pairs(t) do
		for i = 1, n do
			if v == select(i, ...) then
				ret[#ret + 1] = t[k]
				t[k] = nil
			end
		end
	end
	
	return (unpack or table.unpack)(ret)
end

table.empty = function(t)	-- not lume.clear(t)
	local iter = getiter(t)
	for k in iter(t) do t[k] = nil end
	return t
end

table.extend = function(t, ...)
	local n = select("#", ...)
	for i = 1, n do
		local x = select(i, ...)
		if x then
			for k, v in pairs(x) do
				t[k] = v
			end
		end
	end
	return t
end

table.shuffle = function(t)
	local t = table.array(ipairs(t))
	local rtn = {}
	for i = 1, #t do
		local r = math.random(1, i)
		if r ~= i then
			rtn[i] = rtn[r]
		end
		rtn[r] = t[i]
	end
	return rtn
end

table.sort2 = function(t, comp)	-- not lume.sort(t, comp)
	local rtn = table.clone(t)
	if comp then
		if type(comp) == "string" then
			table.sort(rtn, function(a, b) return a[comp] < b[comp] end)
		else
			table.sort(rtn, comp)
		end
	else
		table.sort(rtn)
	end
	return rtn
end

table.array = function(...)
	local t = {}
	for x in ... do t[#t + 1] = x end
	return t
end

table.map = function(t, fn)
	fn = iteratee(fn)
	local rtn = {}
	local iter = getiter(t)
	for k, v in iter(t) do rtn[k] = fn(v) end
	return rtn
end

table.all = function(t, fn)
	fn = iteratee(fn)
	local iter = getiter(t)
	for _, v in iter(t) do
		if not fn(v) then return false end
	end
	return true
end

table.any = function(t, fn)
	fn = iteratee(fn)
	local iter = getiter(t)
	for _, v in iter(t) do
		if fn(v) then return true end
	end
	return false
end

table.reduce = function(t, fn, first)
	local acc = first
	local started = first and true or false
	local iter = getiter(t)
	for _, v in iter(t) do
		if started then
			acc = fn(acc, v)
		else
			acc = v
			started = true
		end
	end
	assert(started, "reduce of an empty table with no first value")
	return acc
end

table.unique = function(t)
	local rtn = {}
	for k in pairs(table.invert(t)) do
		rtn[#rtn + 1] = k
	end
	return rtn
end

table.filter = function(t, fn, retainkeys)
	fn = iteratee(fn)
	local iter = getiter(t)
	local rtn = {}
	if retainkeys then
		for k, v in iter(t) do
			if fn(v) then rtn[k] = v end
		end
	else
		for _, v in iter(t) do
			if fn(v) then rtn[#rtn + 1] = v end
		end
	end
	return rtn
end

table.reject = function(t, fn, retainkeys)
	fn = iteratee(fn)
	local iter = getiter(t)
	local rtn = {}
	if retainkeys then
		for k, v in iter(t) do
			if not fn(v) then rtn[k] = v end
		end
	else
		for _, v in iter(t) do
			if not fn(v) then rtn[#rtn + 1] = v end
		end
	end
	return rtn
end

table.merge = function(...)
	local rtn = {}
	for i = 1, select("#", ...) do
		local t = select(i, ...)
		local iter = getiter(t)
		for k, v in iter(t) do
			rtn[k] = v
		end
	end
	return rtn
end

table.append = function(...)	-- not lume.concat(...)
	local rtn = {}
	for i = 1, select("#", ...) do
		local t = select(i, ...)
		if t ~= nil then
			local iter = getiter(t)
			for _, v in iter(t) do
				rtn[#rtn + 1] = v
			end
		end
	end
	return rtn
end

table.find = function(t, value)
	local iter = getiter(t)
	for k, v in iter(t) do
		if v == value then return k end
	end
	return nil
end

table.match = function(t, fn)
	fn = iteratee(fn)
	local iter = getiter(t)
	for k, v in iter(t) do
		if fn(v) then return v, k end
	end
	return nil
end

table.count = function(t, fn)
	local count = 0
	local iter = getiter(t)
	if fn then
		fn = iteratee(fn)
		for _, v in iter(t) do
			if fn(v) then count = count + 1 end
		end
	else
		if table.isarray(t) then
			return #t
		end
		for _ in iter(t) do count = count + 1 end
	end
	return count
end

table.slice = function(t, i, j)
	i = i and absindex(#t, i) or 1
	j = j and absindex(#t, j) or #t
	local rtn = {}
	for x = i < 1 and 1 or i, j > #t and #t or j do
		rtn[#rtn + 1] = t[x]
	end
	return rtn
end

table.first = function(t, n)
	if not n then return t[1] end
	return table.slice(t, 1, n)
end

table.last = function(t, n)
	if not n then return t[#t] end
	return table.slice(t, -n, -1)
end

table.invert = function(t)
	local rtn = {}
	for k, v in pairs(t) do rtn[v] = k end
	return rtn
end

table.pick = function(t, ...)
	local rtn = {}
	for i = 1, select("#", ...) do
		local k = select(i, ...)
		rtn[k] = t[k]
	end
	return rtn
end

table.keys = function(t)
	local rtn = {}
	assert(type(t) == 'table', "expected t to be a table -- type(t): " .. type(t))
	for k in pairs(t) do
		rtn[#rtn + 1] = k
	end
	return rtn
end

table.clone = function(t)
	local rtn = {}
	for k, v in pairs(t) do rtn[k] = v end
	return rtn
end