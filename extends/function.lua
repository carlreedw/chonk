----------------------------
-- extensions for lua _G
----------------------------
fn = {}

local iscallable = function(x)
	if type(x) == "function" then return true end
	local mt = getmetatable(x)
	return mt and mt.__call ~= nil
end
local memoize_fnkey = {}
local memoize_nil = {}

fn.memoize = function(fn)
	local cache = {}
	return function(...)
		local c = cache
		for i = 1, select("#", ...) do
			local a = select(i, ...) or memoize_nil
			c[a] = c[a] or {}
			c = c[a]
		end
		c[memoize_fnkey] = c[memoize_fnkey] or {fn(...)}
		return unpack(c[memoize_fnkey])
	end
end

fn.time = function(fn, ...)
	local start = os.clock()
	local rtn = {fn(...)}
	return (os.clock() - start), unpack(rtn)
end

fn.fn = function(f, ...)
	assert(iscallable(f), "expected a function as the first argument")
	local args = { ... }
	return function(...)
		local a = table.append(args, { ... })
		return f(unpack(a))
	end
end

fn.once = function(f, ...)
	local g = fn.fn(f, ...)
	local done = false
	return function(...)
		if done then return end
		done = true
		return g(...)
	end
end


fn.call = function(fn, ...)
	if fn then
		return fn(...)
	end
end

fn.isCallable = iscallable