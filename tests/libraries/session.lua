local args = ...
local suite = {
	desc = "Tests for session library",
	tests = {
		{
			desc = "Session can record events to a log file. Default events watched: keypressed/released, mousepressed/released/moved, wheelmoved",
			run = function()
				events:hook(love, session.default_events)
				
				local sess = session()
				sess.print = debug.log
				--[[
				-- example of record using argtable (2nd arg) to define game, version, and events
				sess:record('test_sess.log', {
					events = {'mousepressed', 'mousemoved'},
					game = 'template',
					version = 'acj9vj8'
				})
				]]
				
				sess:record('test_sess.log')
				
				-- assign date or else the comparison will fail
				sess.metadata.date = "[date]"
				
				sess:update(0.17)
				events:trigger('keypressed', 'key', 'scancode', 'isrepeat')
				sess:update(0.13)
				events:trigger('keyreleased', 'z', 'z')
				sess:update(0.16)
				events:trigger('mousepressed', 'x', 'y', 'button' ,'istouch', 'presses')
				sess:update(0.13)
				events:trigger('mousereleased', 'x', 'y', 'button' ,'istouch', 'presses')
				sess:update(0.17)
				events:trigger('mousemoved', 'x', 'y', 'dx', 'dy', 'istouch')
				sess:update(0.19)
				events:trigger('wheelmoved', 'x', 'y')
				
				sess:stop()
				assert(f1 == f2, "Expected test_sess.log to match previously generated docs/test_sess.log")
			end
		}, {
			desc = "Session can load a previously recorded log file for playback",
			run = function()
				local sess = session()
				sess.print = debug.log
				sess:load('docs/test_sess.log')
				local arg
				local e = events('keypressed', function(key)
					if key then arg = true end
				end):register()
				sess:update(0.2)
				sess:stop()
				assert(arg, "session playback didn't trigger event")
			end
		}, {
			desc = "Sessions can be paused, preventing lines from being played back or recorded until the session is resumed",
			run = function()
				local sess = session()
				sess.print = debug.log
				sess:load('docs/test_sess.log')
				local arg
				local e = events('keypressed', function(key)
					if key then arg = true end
				end):register()
				sess:pause()
				sess:update(0.2)
				assert(not arg, "session playback triggered event while paused")
				sess:play()
				sess:update(0.2)
				sess:stop()
				assert(arg, "session playback didn't trigger event after resuming")
				
				sess = session()
				sess.print = debug.log
				sess:record('test_sess.log')
				sess:update(0.17)
				events:trigger('keypressed', 'key', 'scancode', 'isrepeat')
				sess:pause()
				local filestring = love.filesystem.read('test_sess.log')
				
				-- do another event
				sess:update(0.17)
				events:trigger('keypressed', 'key', 'scancode', 'isrepeat')
				assert(love.filesystem.read('test_sess.log') == filestring, "session wrote to file while paused")
				
				sess:play()
				
				-- do another event
				sess:update(0.17)
				events:trigger('keypressed', 'key', 'scancode', 'isrepeat')
				assert(love.filesystem.read('test_sess.log') ~= filestring, "session didn't write to file after resuming")
				sess:stop()
			end
		}, {
			desc = "Loading a log file stops recording, and recording to a new file stops playback",
			run = function()
				local sess = session()
				sess.print = debug.log
				sess:record('test_sess.log')
				sess:update(0.17)
				events:trigger('keypressed', 'key', 'scancode', 'isrepeat')
				
				sess:load('test_sess.log')
				
				sess:update(0.17)
				events:trigger('keypressed', 'key', 'scancode', 'isrepeat')
				filestring = love.filesystem.read('test_sess.log')
				
				local filestring2 = love.filesystem.read('test_sess.log')
				assert(filestring2 == filestring, "session wrote to file after :load call")
				
				sess:stop()
				sess:load('test_sess.log')
				sess:record('test_sess.log')
				local e = events('keypressed', function(key)
					if key == 'key' then error('playback still going after call to :record') end
				end)
				
				sess:update(0.17)
				events:trigger('keypressed', 'not key', 'scancode', 'isrepeat')
				sess:stop()
			end
		}, {
			desc = "You can overwrite session.write or session.read to change or extend the behavior of a session",
			run = function()
				local sess = session()
				sess.print = debug.log
				sess.write = function(ts, ename, ...)
					local line = tostring(math.round(ts, 3))
					
					-- convert ename to integer
					local event_index = table.find(sess.default_events, ename)
					
					if event_index then
						line = line .. ', ' .. tostring(event_index)
					else
						return
					end
					
					line = line .. ', ' .. table.concat({...}, ', ')
					return line .. "\r\n"
				end
				sess.read = function(line)
					local ts, ename, args = nil, nil, {}
					for token in string.gmatch(line, '[^,]+') do
						local token = string.trim(token)
						if tonumber(token) then token = tonumber(token) end
						
						if not ts then
							ts = token
						elseif not ename then
							ename = session.default_events[token]
						else
							args[#args + 1] = token
						end
					end
					
					return ts, ename, unpack(args)
				end
				sess:record("test_sess2.log")
				
				-- assign date or else the comparison will fail
				sess.metadata.date = "[date]"
				
				sess:update(0.17532)
				events:trigger('keypressed', 'key', 'scancode', 'isrepeat')
				sess:update(0.13545)
				events:trigger('keyreleased', 'z', 'z')
				sess:update(0.166759)
				events:trigger('mousepressed', 'x', 'y', 'button' ,'istouch', 'presses')
				sess:update(0.132457)
				events:trigger('mousereleased', 'x', 'y', 'button' ,'istouch', 'presses')
				sess:update(0.1796)
				events:trigger('mousemoved', 'x', 'y', 'dx', 'dy', 'istouch')
				sess:update(0.1962457)
				events:trigger('wheelmoved', 'x', 'y')
				
				sess:stop()
				
				local f1, f2 = love.filesystem.read('docs/test_sess2.log'), love.filesystem.read('test_sess2.log')
				if f1 ~= f2 then
					debug.log("f1:\n" .. f1 .. "\n\nf2:\n" .. f2)
					for i = 1, #f1 do
						if f2:sub(i, i) ~= f1:sub(i, i) then
							print('string different at index ' .. i .. ', chars: ' .. f1:sub(i, i) .. ', ' .. f2:sub(i, i))
							break
						end
					end
				end
				assert(f1 == f2, "Expected test_sess2.log to match previously generated docs/test_sess2.log")
				
				sess:load('docs/test_sess2.log')
				local arg
				local event = events('keypressed', function(key)
					arg = key
				end):register()
				sess:update(0.2)
				assert(arg == 'key', "expected sess.read to convert '1' eventname to keypressed")
				
				
				-- unhook events
				events:unhook()
			end
		}
	}
}

return suite