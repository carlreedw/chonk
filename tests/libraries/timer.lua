local args = ...
local suite = {
	desc = "Tests for timer library",
	tests = {
		{
			desc = "A timer can schedule an action to happen after a delay",
			run = function()
				local callbackWasCalled
				local callback = function()
					callbackWasCalled = true
				end
				local timer = timer()
				
				timer:after(1, callback)
				
				timer:update(1)
				
				assert(callbackWasCalled, "Scheduled :after callback wasn't triggered by timer")
			end
		},
		{
			desc = "A timer can schedule an action to happen with a periodic frequency",
			run = function()
				local sum = 0
				local callback = function()
					sum = sum + 1
				end
				local timer = timer()
				timer:every(1, callback)
				
				timer:update(.5)
				timer:update(.5)
				assert(sum == 1, "Scheduled :every callback wasn't triggered by timer")
				
				timer:update(1)
				
				assert(sum == 2, "Scheduled :every callback wasn't triggered by timer a 2nd time")
				
				timer:update(2.99)
				
				assert(sum == 4, "Scheduled :every callback wasn't triggered twice in one :update call")
			end
		},
		{
			desc = "A timer can forward arguments to an :after or :every callback",
			run = function()
				local sum = 0
				local f = function(a1, a2, a3)
					sum = sum + a1+a2+a3
				end
				local timer_inst = timer()
				timer_inst:after(1, f, 1, 2, 3)
				
				timer_inst:update(1)
				
				assert(sum == 6, "Not all arguments were passed to callback -- var: " .. debug.tostring(var))
				
				sum = 0
				local timer_inst = timer()
				
				timer_inst:every(1, f, 1, 2, 3)
				
				timer_inst:update(1)
				
				assert(sum == 6, "Not all arguments were passed to callback")
			end
		},
		{
			desc = "All timer library functions return the timer as the first return value to allow for function chaining",
			run = function()
				local foo = function() end
				local timer = timer()
				
				assert(timer:after(1, foo) == timer, "'after' did not return timer")
				assert(timer:every(1, foo) == timer, "'every' did not return timer")
				assert(timer:update(1) == timer, "'update' did not return timer")
				assert(timer:reset() == timer, "'reset' did not return timer")
				assert(timer:trigger() == timer, "'trigger' did not return timer")
				assert(timer:setundo(foo) == timer, "'setundo' did not return timer")
				assert(timer:undo() == timer, "'undo' did not return timer")
			end
		},
		{
			desc = "Calling a timer's :after or :every function replaces the scheduled action of any previous :after or :every call",
			run = function()
				local var
				local foo = function() end
				local bar = function() var = true end
				local t = timer()
				t:after(1, foo)
				t:after(1, bar)
				
				t:update(1)
				
				assert(var, "timer:after call didn't replace timer's callback function")
				
				local t = timer()
				t:every(1, foo)
				t:every(1, bar)
				
				t:update(1)
				
				assert(var, "timer:every call didn't replace timer's callback function")
			end
		},
		{
			desc = "Timers can have their callbacks triggered (with the previously supplied arguments) at any time",
			run = function()
				local var
				local foo = function() var = true end
				local timer = timer()
				timer:after(1, foo)
				
				timer:trigger()
				
				assert(var, "Timer's :trigger call didn't trigger callback function")
			end
		},
		{
			desc = "Timers can be configured to undo their callback actions when they are updated with a negative time duration",
			run = function()
				local flag
				local foo = function(bool)
					flag = bool
				end
				local bar = function(bool)
					flag = bool
				end
				local timer = timer()
				timer:after(1, foo, true)
				timer:setundo(bar, false)
				
				timer:update(1)
				assert(flag, "timer:update call didn't trigger callback set by :after")
				timer:update(-1)
				
				assert(flag == false, "Undo function specified was not called after negative time update")
			end
		},
		{
			desc = "Timers keep track of recent and total callback (and undo) counts, how much time has elapsed, and if an :after action is done",
			run = function()
				local sum = 0
				local foo = function(arg)
					sum = sum + arg
				end
				local timer = timer()
				timer:after(1, foo, 1)
				
				timer:update(.4)
				assert(timer.elapsed == .4, "timer has wrong .elapsed after updatement -- .elapsed=" .. timer.elapsed)
				
				timer:update(.6)
				
				assert(timer.done, "timer's .done property not set to true upon 'after' callback completion -- .done=" .. tostring(timer.done))
				assert(timer.trigger_count == 1, "timer's didn't add to .trigger_count after updatement -- .trigger_count=" .. timer.trigger_count)
				
				timer:every(1, foo, 1)
				
				assert(timer.trigger_count == 0, "timer's .trigger_count wasn't reset after :every call")
				
				timer:update(1)
				timer:update(2)
				
				assert(timer.trigger_count == 3, "timer has wrong total .trigger_count after call to :every and :update -- .trigger_count=" .. timer.trigger_count)
				assert(timer.recent_trigger_count == 2, "timer has wrong .recent_trigger_count after call to :every and :update -- .recent_trigger_count=" .. timer.recent_trigger_count)
				
				timer:setundo(foo, -1)
				
				timer:update(-1)
				timer:update(-2)
				
				assert(timer.undo_count == 3, "timer has wrong total .undo_count after call to :every and :update -- .undo_count=" .. timer.undo_count)
				assert(timer.recent_undo_count == 2, "timer has wrong .recent_undo_count after call to :every and :update -- .recent_undo_count=" .. timer.recent_undo_count)
			end
		},
		{
			desc = "Timers can reset their public properties while keeping any scheduled callback and associated arguments",
			run = function()
				local sum = 0
				local foo = function(arg)
					sum = sum + arg
				end
				local timer = timer()
				timer:after(1, foo, 1)
				
				timer:update(.4)
				
				assert(timer.elapsed == .4, "timer has wrong .elapsed after updatement -- .elapsed=" .. timer.elapsed)
				
				timer:update(.6)
				
				assert(timer.done, "timer's .done property not set to true upon 'after' callback completion -- .done=" .. tostring(timer.done))
				assert(timer.trigger_count == 1, "timer's didn't add to .trigger_count after updatement -- .trigger_count=" .. timer.trigger_count)
				
				timer:reset()
				
				assert(timer.elapsed == 0, "call to :reset did not erase .elapsed property")
				assert(timer.done == nil, "call to :reset did not erase .done property")
				assert(timer.trigger_count == 0, "call to :reset did not erase .trigger_count property")
				
				timer:every(1, foo, 1)
				
				timer:update(1)
				timer:update(2)
				
				assert(timer.trigger_count == 3, "timer has wrong total .trigger_count after call to :every and :update -- .trigger_count=" .. timer.trigger_count)
				assert(timer.recent_trigger_count == 2, "timer has wrong .recent_trigger_count after call to :every and :update -- .recent_trigger_count=" .. timer.recent_trigger_count)
				
				timer:reset()
				
				assert(timer.trigger_count == 0, "call to :reset did not erase .trigger_count property")
				assert(timer.recent_trigger_count == 0, "call to :reset did not erase .recent_trigger_count property")
				
				timer:setundo(foo, -1)
				
				timer:update(-1)
				timer:update(-2)
				
				assert(timer.undo_count == 3, "timer has wrong total .undo_count after call to :every and :update -- .undo_count=" .. timer.undo_count)
				assert(timer.recent_undo_count == 2, "timer has wrong .recent_undo_count after call to :every and :update -- .recent_undo_count=" .. timer.recent_undo_count)
				
				timer:reset()
				
				assert(timer.undo_count == 0, "call to :reset did not erase .undo_count property")
				assert(timer.recent_undo_count == 0, "call to :reset did not erase .recent_undo_count property")
			end
		}
	}
}

return suite