--[[
	agenda:
		apply gravity to player and chonk
		implement collision detection/resolution between player and chonk
		allow player to move left/right on chonk and stay upright
]]

local state = {}

function state:draw()
	self.world:draw()
end

function state:init()
	gravity = 9.81
	self.clocks = {}
	self.world = World()
	
	self.world:addChonk()
	local sw, sh = lg.getDimensions()
	chonk = self.world.chonks[1]
	chonk.x = sw/2
	chonk.y = sh/2
	
	self.world:addPlayer({x = chonk.x, y = chonk.y - chonk.maxrad * 1.1})
	player = self.world.players[1]
	
end	

function state:listen(bool)
	if bool then
		self.events = {}
		self.events.reset = events("keyreleased", function(key)
			state:init()
		end):register()
	else
		for name, e in pairs(self.events) do
			e:remove()
		end
		self.events = nil
	end
end

function state:update(dt)
	-- local dt = dt / 100
	for name, clock in pairs(self.clocks) do
		clock:update(dt)
	end
	
	-- apply world gravity to bodies
	self.world:update(dt)
	
	-- update object positions
	player:update(dt)
end

--////////////////////
--	World
--////////////////////
World = {}
World.new = function()
	local world = {}
	world.gravity = 9.81
	world.chonks = {}
	world.players = {}
	world.systems = {}
	world.systems.gravity = {}
	setmetatable(world.systems.gravity, table.weakmetatable)
	
	setmetatable(world, {__index = World})
	return world
end

function World:addChonk(chonk)
	local chonk = chonk or Chonk()
	table.insert(self.chonks, chonk)
	table.insert(self.systems.gravity, chonk)
end

function World:addPlayer(config)
	local player = Player(config)
	table.insert(self.players, player)
	table.insert(self.systems.gravity, player)
end

function World:isGravitational(body)
	if body.static then return false end
	if not fn.isCallable(body.applyGravity) then return false end
	if not (body.x or body.y or body.mass) then return false end
	return true
end

function World:update()
	local function calcgravity(body)
		local fx, fy = 0, 0
		local m1, x1, y1 = body.mass, body.x, body.y
		
		-- loop through other bodies
		for i, body2 in ipairs(self.systems.gravity) do
			if self:isGravitational(body2) then
				local dx, dy = body2.x - x1, body2.y - y1
				local r2 = dx*dx - dy*dy
				if r2 ~= 0 then
					local grav_force = self.gravity * m1 * body2.mass / r2
					local theta = math.atan2(dy, dx)
					fx = fx + math.cos(theta) * grav_force
					fy = fy + math.sin(theta) * grav_force
					
					-- enforce gravity max force
					fx = math.min(math.abs(fx), 50) * math.sign(fx)
					fy = math.min(math.abs(fy), 50) * math.sign(fy)
				end
			end
		end
		
		-- apply force
		return {fx = fx, fy = fy}
	end
	
	for i, body in ipairs(self.systems.gravity) do
		if self:isGravitational(body) then
			local grav = calcgravity(body)
			body:applyGravity(grav.fx, grav.fy)
		end
	end
end

function World:draw()
	for i, chonk in ipairs(self.chonks) do
		chonk:draw()
	end
	
	for i, player in ipairs(self.players) do
		player:draw()
	end
end

--[[
	world needs to test players closer than chonk.maxrad
]]

setmetatable(World, {
	__call = function(World)
		return World.new()
	end
})

--////////////////////
--	Chonk
--////////////////////
Chonk = {}

function Chonk:applyGravity(fx, fy)
	self.gravity = {fx = fx, fy = fy}
end

function Chonk:draw()
	lg.setColor(.2, .5, .8)
	lg.draw(self.mesh, self.x, self.y)
	
	lg.setColor(self.color)
	lg.circle('line', self.x, self.y, self.maxrad, self.vertcount*4)
end

Chonk.new = function(chonkconfig)
	local chonk = chonkconfig or {}
	
	chonk.ax = chonk.ax or 0
	chonk.ay = chonk.ay or 0
	chonk.vx = chonk.vx or 0
	chonk.vy = chonk.vy or 0
	chonk.x = chonk.x or 0
	chonk.y = chonk.y or 0
	chonk.color = chonk.color or {0.66, 0.7, 0.782}
	chonk.verts = {}
	
	local vertcount = love.math.random(8, 28)
	local rad = vertcount * love.math.random(3, 3 + math.log(vertcount))
	local theta = (1/vertcount) * math.pi * 2
	local maxrad = rad * 2
	local actual_max = 0
	for i = 1, vertcount do
		local angle = i * theta + (love.math.random() - 0.5) * 0.25 * theta
		local range = 1
		local x = rad * math.cos(angle)
		local y = rad * math.sin(angle)
		local noise_args = {x = i * x/maxrad*29.980, y = i * y/maxrad * 29.024898}
		local noisex = (love.math.noise(noise_args.x, noise_args.y, i) + range)
		local noisey = (love.math.noise(noise_args.y, noise_args.x, i) + range)
		x, y = x * noisex, y * noisey
		table.push(chonk.verts, {
			-- x, y, u, v, r, g, b, a
			x, y,
			x/maxrad, y/maxrad,
			1, 1, 1, 1
		})
		actual_max = math.max(actual_max, math.sqrt(x*x + y*y))
	end
	chonk.mesh = lg.newMesh(chonk.verts)
	
	chonk.vertcount = vertcount
	chonk.theta = theta
	chonk.maxrad = actual_max
	chonk.rad = rad
	
	chonk.mass = (vertcount * .25) + 10 * 500
	
	setmetatable(chonk, {__index = Chonk})
	return chonk
end

function Chonk:update(dt)
	if self.gravity then
		self.ax = self.ax + self.gravity.fx / self.mass
		self.ay = self.ay - self.gravity.fy / self.mass
		self.vx = self.vx + self.ax*dt
		self.vy = self.vy - self.ay*dt
		self.x = self.x + self.vx*dt
		self.y = self.y - self.vy*dt
	end
end

setmetatable(Chonk, {
	__call = function(Chonk, config)
		return Chonk.new(config)
	end
})

--////////////////////
--	Camera
--////////////////////

--////////////////////
--	Player
--////////////////////
Player = {}

function Player:draw()
	lg.setColor(0.5, 0.8, 0.82)
	local x, y, w, h = self.x, self.y, self.w, self.h
	lg.rectangle('fill', x - w/2, y - h, w, h)
	
	-- debug feet
	lg.setColor(1, 0, 1)
	lg.circle('line', self.x, self.y, 4, 12)
end

function Player.new(config)
	local player = config or {}
	
	player.mass = player.mass or 100
	player.ax = player.ax or 0
	player.ay = player.ay or 0
	player.vx = player.vx or 0
	player.vy = player.vy or 0
	player.x = player.x or 0
	player.y = player.y or 0
	player.w, player.h = player.w or 10, player.h or 22
	player.verts = player.verts or {}
	local x, y, w, h = player.x, player.y, player.w, player.h
	if (not next(player.verts)) then
		table.insert(player.verts, {
			-- x, y, u, v, r, g, b, a
			-w/2, -h,
			0, 0,
			1, 1, 1, 1
		})
		table.insert(player.verts, {
			-- x, y, u, v, r, g, b, a
			w/2, -h,
			0, 0,
			1, 1, 1, 1
		})
		table.insert(player.verts, {
			-- x, y, u, v, r, g, b, a
			w/2, 0,
			0, 0,
			1, 1, 1, 1
		})
		table.insert(player.verts, {
			-- x, y, u, v, r, g, b, a
			-w/2, 0,
			0, 0,
			1, 1, 1, 1
		})
		player.mesh = lg.newMesh(player.verts)
	end
	player.mesh = player.mesh or lg.newMesh(player.verts)
	
	setmetatable(player, {__index = Player})
	return player
end

function Player:applyGravity(fx, fy)
	self.gravity = {fx = fx, fy = fy}
end

function Player:update(dt)
	if self.gravity then
		self.ax = self.ax + self.gravity.fx / self.mass
		self.ay = self.ay - self.gravity.fy / self.mass
		self.vx = self.vx + self.ax*dt
		self.vy = self.vy - self.ay*dt
		self.x = self.x + self.vx*dt
		self.y = self.y - self.vy*dt
	end
	
	debug.log(string.format("update 1 player (gx, gy, ax, ay, vx, vy, x, y): (%s, %s, %s, %s, %s, %s, %s, %s)", self.gravity.fx, self.gravity.fy, self.ax, self.ay, self.vx, self.vy, self.x, self.y))
end

setmetatable(Player, {
	__call = function(Player, config)
		return Player.new(config)
	end
})

--////////////////////

return state