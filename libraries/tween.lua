local tween = {}

tween.easings = {
	linear = function(x)
		return x
	end,
	quad = function(x)
		return x*x
	end,
	cubic = function(x)
		return x*x*x
	end,
	quart = function(x)
		return x*x*x*x
	end,
	quint = function(x)
		return x*x*x*x*x
	end,
	expo = function(x)
		return 2 ^ (10 * (x - 1))
	end,
	sine = function(x)
		return -math.cos(x * (math.pi * .5)) + 1
	end,
	circ = function(x)
		return -(math.sqrt(1 - (x*x)) - 1)
	end,
	back = function(x)
		return 2.7*x*x*x - 1.7*x*x
	end,
	elastic = function(x)
		local a, b, p = 2, 10, 0.3
		local amp = a^(-b*x)*(1-x^15)
		local osc = math.sin( (x - p/4)*2*math.pi/p )
		return amp*osc + 1
	end
}

local easings = tween.easings

local function is_callable(x)
	if type(x == 'function') then return true end
	local mt = getmetatable(x)
	if mt.__call then return true end
	return false
end

setmetatable(tween, {__call = function(class, target, final, time)
	local instance = {
		label = 'tween'
	}
	
	setmetatable(instance, {
		__index = tween
	})
	
	if target and final then
		instance:tween(target, final, time)
	end
	
	return instance
end})

function tween:tween(target, final, time)
	assert(type(target) == 'table', "The first argument to tween() or tween:tween() must be a target table")
	assert(type(final) == 'table', "The second argument to tween() or tween:tween() must be a final values table")
	
	self.values = {}
	for k, v in pairs(final) do
		if type(v) == 'number' and type(target[k]) == 'number' then
			self.values[k] = target[k]
		end
	end
	assert(next(self.values), "The supplied final values table (2nd arg) must have a key => number pair that correlates with a key => number pair in the target table (1st arg)")
	if time then
		assert(type(time) == 'number' and time > 0, "If tween is supplied a 3rd argument, it must be a positive number denoting how long the tween takes after any delay")
	end
	self.target = target
	self.final = final
	self.time = time or 1
	
	self.elapsed = 0
	return self
end

function tween:update(dt)
	local dt = dt
	local function exit()
		if self.after_f then
			self.after_f((unpack or table.unpack)(self.after_args or {}))
		end
		return self
	end
	
	if not dt then
		return exit()
	end
	
	-- always call a set before_f
	if self.before_f then
		self.before_f((unpack or table.unpack)(self.before_args or {}))
	end
	
	self.elapsed = self.elapsed + dt
	
	-- have we passed delay?
	if self.delay_time then
		if self.elapsed < self.delay_time then
			return exit()
		end
	end
	
	local total_time = (self.delay_time or 0) + self.time
	
	if self.elapsed >= total_time and not self.done then
		self:finish()
		return exit()
	else
		for k, vf in pairs(self.final) do
			local diff = vf - self.values[k]
			local progress = (self.elapsed - (self.delay_time or 0)) / self.time
			
			local eased
			if self.invert then
				eased = 1 - (self.easing_f or easings.linear)(1 - progress)
			else
				eased = (self.easing_f or easings.linear)(progress)
			end
			if self.target[k] then
				self.target[k] = self.values[k] + eased*diff
			end
		end
		return exit()
	end
end

function tween:oncomplete(f, ...)
	self.complete_f = f
	self.complete_args = {...}
	return self
end

function tween:before(f, ...)
	self.before_f = f
	self.before_args = {...}
	return self
end

function tween:after(f, ...)
	self.after_f = f
	self.after_args = {...}
	return self
end

function tween:delay(time)
	self.delay_time = time
	return self
end

function tween:ease(easing, invert)
	self.invert = invert
	if type(easing) == 'string' then
		assert(easings[easing], "easing function '" .. easing .. '" not found in tween library easings table')
		self.easing_f = easings[easing]
	elseif type(easing) == 'function' then
		self.easing_f = easing
	end
	return self
end

function tween:finish(...)
	-- set tween values to final
	for k, v in pairs(self.final) do
		if self.target[k] then
			self.target[k] = v
		end
	end
	
	self.done = true
	
	-- use newly supplied args -- if none supplied, use stored args, else empty args table
	local args = {...}
	args = (next(args) and args) or self.complete_args or {}
	
	if self.complete_f then
		self.complete_f((unpack or table.unpack)(args))
	end
	return self
end

function tween:reset()
	for k, v in pairs(self.values) do
		if self.target[k] then
			self.target[k] = v
		end
	end
	
	self.elapsed = 0
	
	return self
end

return tween