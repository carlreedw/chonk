lg = love.graphics

function loadstuff()
	-- make mesh
	verts = {}
	local n = 16
	local sw, sh = lg.getDimensions()
	local radius = sh*0.25
	for i = 1, n do
		local rads = math.pi * 2.0 * (i - 1) / n
		local x = math.cos(rads) * radius
		local y = math.sin(rads) * radius
		verts[i] = {x, y, x/sw, y/sh, 1, 1, 1, 1}
	end
	mesh = lg.newMesh(verts)
	
	-- generate and set texture?
end

loadstuff()


function love.update(dt)
	game:update(dt)
end

function love.draw()
	game:draw(dt)
	
	local sw, sh = lg.getDimensions()
	lg.setColor(1, 1, 1)
	lg.draw(mesh, sw/2, sh/2)
end



